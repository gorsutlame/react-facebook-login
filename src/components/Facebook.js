import React , {useState, useEffect} from 'react';
import FacebookLogin from "react-facebook-login";

const FacebookLoginButton = () => {
    const [isLoggedIn, setLoggedIn] = useState(false);
    const [user, setUser] = useState({
        userID: "",
        name: "",
        email: "",
        picture: ""
    });
    const [fbContent, setFbContent] = useState("");

    const responseFacebook = response => {
        console.log("load", response);

        if (response.status == "unknown") return;

        setUser({
          userID: response.userID,
          name: response.name,
          email: response.email,
          picture: response.picture.data.url
        });
        setLoggedIn(true)
    }

    const FacebookLogout = () =>{
      setLoggedIn(false)
      window.FB.logout()
    }

    useEffect(() => {
        if (isLoggedIn){
            setFbContent(
                <div
                  style={{
                    width: "400px",
                    margin: "auto",
                    background: "#f4f4f4",
                    padding: "20px"
                  }}
                >
                  <img src={user.picture} alt={user.name} />
                  <h2>Welcome {user.name}</h2>
                  Email: {user.email}
                  <p>
                    <button onClick={FacebookLogout}>Logout</button>
                  </p>
                </div>
            );
        }else{
            setFbContent(
                <FacebookLogin
                  appId="1218637328513868"
                  autoLoad={true}
                  fields="name,email,picture"
                //   onClick={this.componentClicked}
                  callback={responseFacebook}
                />
              );
              console.log(1, fbContent)
        }

    }, [isLoggedIn]);
    return <div>{fbContent}</div>
}

export default FacebookLoginButton;