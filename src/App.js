import React from 'react';
import logo from './logo.svg';
import './App.css';

import FacebookLoginButton from './components/Facebook';

function App() {
  return (
    <div className="App">
      <div className="App-info">
          <h1>FACEBOOK EXAMPLE LOGIN</h1>
          <FacebookLoginButton />
      </div>
    </div>
  );
}

export default App;
